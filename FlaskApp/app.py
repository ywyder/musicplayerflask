from flask import Flask, render_template, request
from mpd import MPDClient
import os
import json
import logging


app= Flask(__name__)

# start mpd
os.system("mpd ~/workbench/MusicPlayerFlask/FlaskApp/static/mpd/mpd.conf")
os.system("mpc play")
playerActive = False

client = MPDClient()
client.timeout = 5
client.connect("localhost", 6600)
client.setvol(12)


#load station from JSON and add to playlist
client.clear()
stationlist = json.load(open('./static/radiostation.json'))

for station in stationlist:
    client.add(station["url"])


# render start view
@app.route("/")
def main():
    return render_template('index.html')

# defintion of control functions
@app.route('/play',methods=["POST"])
def play():
    try:
        client.play()
        return ('',204)
    except:
        return ('error', 404)


@app.route('/next', methods=["POST"])
def playNext():
    try:
        client.next()
        return ('', 204)
    except:
        pass


@app.route('/prev',methods=["POST"])
def playPrevious():
    try:
        client.previous()
        return ('', 204)
    except:
        pass

@app.route('/volume/<volumeValue>', methods=["POST"])
def setVolume(volumeValue):
    try:
        client.setvol(volumeValue)
        return ('', 204)
    except:
        pass


# function for current station and song
@app.route('/currentsong', methods=["GET"])
def current():
    try:
        currentInfo = client.currentsong()
        currentSong = currentInfo["title"]
        currentStation = currentInfo["name"]
        return render_template('index.html', currentSong=currentSong, currentStation=currentStation)
    except:
        return render_template('index.html', currentSong='n/a', currentStation="")

if __name__ == "__main__":
    app.run(host="127.0.0.1")
